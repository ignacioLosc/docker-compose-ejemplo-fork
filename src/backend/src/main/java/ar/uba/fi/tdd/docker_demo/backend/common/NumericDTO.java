package ar.uba.fi.tdd.docker_demo.backend.common;

import java.math.BigInteger;

public record NumericDTO(
        BigInteger result
) {}
