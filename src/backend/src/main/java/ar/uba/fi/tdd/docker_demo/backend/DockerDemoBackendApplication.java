package ar.uba.fi.tdd.docker_demo.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerDemoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerDemoBackendApplication.class, args);
	}

}
