window.onload = function() {
	const button = document.getElementById('button');
	const valueElement = document.getElementById('value');

	button.addEventListener("click", async function(event) {
		valueElement.innerHTML = "Loading";
		
		const resultValue = await fetch(`${_env_.API_URL}/counter`, {
			method: "POST"
		});
		
		const value = (await resultValue.json()).result;
		valueElement.innerHTML = value;
	});
}
